import { Component } from '@angular/core';
import { Item } from './shared/item';
import { items } from './shared/data';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  items = items;

  create(name) {
    const item = new Item(name);
    this.items.push(item);
  }
}
