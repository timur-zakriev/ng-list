import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Item } from '../shared/item';


@Component({
  selector: 'list-item',
  templateUrl: 'list-item.component.html',
  styleUrls: ['list-item.component.css']
})

export class ListItem {
  @Input() item: Item;
  @Output() delete = new EventEmitter();

  onDelete() {
    this.delete.emit(this.item);
  }
}